package detector

import "testing"

func TestDetect(t *testing.T) {
	type args struct {
		phrase string
		lans   []string
	}
	langs := []string{"fin","eng","est","swe" }
	tests := []struct {
		name  string
		args  args
		want  string
		want1 bool
	}{
		// TODO: Add test cases.
		{"test0", args{"Suomalaiset seksivideot isotissiset",langs},  "fin",true},
		{"testRUS", args{"Великолепный паркет из 3 пород дерева",langs}, "mkd",false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := Detect(tt.args.phrase, tt.args.lans...)
			if got != tt.want {
				t.Errorf("Detect() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Detect() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

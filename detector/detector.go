package detector

import (
	"github.com/abadojack/whatlanggo"
)

func Detect(phrase string, lans ...string) (string,bool) {

	info := whatlanggo.Detect(phrase)

	language := whatlanggo.LangToString(info.Lang)

	// println(language)

	for _, lan := range lans {

		if lan == language {

			return  language,true

		}

	}

	return language,false

}

vgo build -o v0/cmd/server/server v0/cmd/server/main.go
vgo build -o v0/cmd/utils/makedb v0/cmd/utils/makedb.go

autotag -v -b 2-version-solution
autotag -v -b 1-start -p rc -T datetime
git-chglog --output CHANGELOG.md

git push --tags


git push --follow-tags

vgo test -v ./v1/detector/...
vgo test -v all


#Delete local tags.
git tag -d $(git tag -l)
#Fetch remote tags.
git fetch
#Delete remote tags.
git push origin --delete $(git tag -l) # Pushing once should be faster than multiple times
#Delete local tags.
git tag -d $(git tag -l)



